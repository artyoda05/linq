﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Linq.Objects;

namespace Linq
{
    public static class Tasks
    {
        //The implementation of your tasks should look like this:
        public static string TaskExample(IEnumerable<string> stringList)
        {
            return stringList.Aggregate<string>((x, y) => x + y);
        }

        #region Low

        public static IEnumerable<string> Task1(char c, IEnumerable<string> stringList)
        {
            return stringList
                .Where(x => x.StartsWith(c.ToString()) && x.EndsWith(c.ToString()) && x.Length > 1);
        }

        public static IEnumerable<int> Task2(IEnumerable<string> stringList)
        {
            return stringList
                .Select(str => str.Length)
                .OrderBy(x => x);
        }

        public static IEnumerable<string> Task3(IEnumerable<string> stringList)
        {
            return stringList
                .Select(str => str.First().ToString() + str.Last().ToString());
        }

        public static IEnumerable<string> Task4(int k, IEnumerable<string> stringList)
        {
            return stringList
                .Where(str => str.Length == k && char.IsDigit(str.Last()))
                .OrderBy(str => str);
        }

        public static IEnumerable<string> Task5(IEnumerable<int> integerList)
        {
            return integerList
                .Where(x => x % 2 != 0)
                .OrderBy(x => x)
                .Select(x => x.ToString());
        }

        #endregion

        #region Middle

        public static IEnumerable<string> Task6(IEnumerable<int> numbers, IEnumerable<string> stringList)
        {
            return numbers
                .Select(n => stringList
                    .FirstOrDefault(x => (char.IsDigit(x[0])) && (x.Length == n)) ?? "Not found");
        }

        public static IEnumerable<int> Task7(int k, IEnumerable<int> integerList)
        {
            return integerList
                .Except(integerList.Skip(k))
                .Where(x => x % 2 == 0)
                .Reverse();
        }

        public static IEnumerable<int> Task8(int k, int d, IEnumerable<int> integerList)
        {
            return integerList
                .TakeWhile(x => x <= d)
                .Union(integerList.Skip(k))
                .OrderByDescending(x => x);
        }

        public static IEnumerable<string> Task9(IEnumerable<string> stringList)
        {
            return stringList
                .GroupBy(x => x.FirstOrDefault())
                .Select(g => g.Aggregate(
                    (0, g.Key),
                    (agg, next) => (agg.Item1 + next.Length, g.Key)))
                .OrderByDescending(x => x.Item1).ThenBy(x => x.Key)
                .Select(x => $"{x.Item1}-{x.Key}");
        }

        public static IEnumerable<string> Task10(IEnumerable<string> stringList)
        {
            return stringList
                .OrderBy(str => str)
                .GroupBy(str => str.Length)
                .Select(g => g.Aggregate("", (s, s1) => s + char.ToUpper(s1.Last())))
                .OrderByDescending(s => s.Length);
        }

        #endregion

        #region Advance

        public static IEnumerable<YearSchoolStat> Task11(IEnumerable<Entrant> nameList)
        {
            return nameList
                .GroupBy(x => x.Year)
                .Select(g => new YearSchoolStat
                {
                    Year = g.Key,
                    NumberOfSchools = g
                        .Select(x => x.SchoolNumber)
                        .Distinct()
                        .Count()
                })
                .OrderBy(g => g.NumberOfSchools)
                .ThenBy(g => g.Year);
        }

        public static IEnumerable<NumberPair> Task12(IEnumerable<int> integerList1, IEnumerable<int> integerList2)
        {
            return integerList1
                .Join(integerList2,
                    x => x % 10,
                    y => y % 10,
                    (x, y) => new NumberPair{Item1 = x, Item2 = y})
                .OrderBy(i => i.Item1)
                .ThenBy(i => i.Item2);
        }

        public static IEnumerable<YearSchoolStat> Task13(IEnumerable<Entrant> nameList, IEnumerable<int> yearList)
        {
            return nameList
                .Join(yearList,
                    x => x.Year,
                    y => y,
                    (x, y) => (x.SchoolNumber, x.Year))
                .GroupBy(x => x.Year)
                .Select(g => new YearSchoolStat
                {
                    Year = g.Key,
                    NumberOfSchools = g
                        .Select(g => g.SchoolNumber)
                        .Distinct()
                        .Count()
                })
                .OrderBy(x => x.NumberOfSchools)
                .ThenBy(x => x.Year);
        }

        public static IEnumerable<MaxDiscountOwner> Task14(IEnumerable<Supplier> supplierList,
                IEnumerable<SupplierDiscount> supplierDiscountList)
        {
            return supplierDiscountList
                .Join(
                    supplierList,
                    x => x.SupplierId,
                    y => y.Id,
                    (x, y) => new MaxDiscountOwner
                    {
                        Discount = x.Discount,
                        Owner = y,
                        ShopName = x.ShopName
                    })
                .GroupBy(x => x.ShopName)
                .Select(g => g.Aggregate((agg, next) =>
                    next.Discount > agg.Discount ? next :
                    next.Discount == agg.Discount && next.Owner.Id < agg.Owner.Id ? next : agg))
                .OrderBy(x => x.ShopName);
        }

        public static IEnumerable<CountryStat> Task15(IEnumerable<Good> goodList,
            IEnumerable<StorePrice> storePriceList)
        {
            return goodList
                .GroupJoin(
                    storePriceList,
                    x => x.Id,
                    y => y.GoodId,
                    (x, y) => new {Country = x.Country, StorePrices = y})
                .GroupBy(x => x.Country)
                .Select(x => x.Aggregate((agg, next) =>
                    new
                    {
                        agg.Country,
                        StorePrices = agg.StorePrices.Union(next.StorePrices)
                    }))
                .Select(x => new CountryStat
                {
                    Country = x.Country,
                    MinPrice = x.StorePrices.Aggregate(0.0,
                        (agg, next) => agg >= next.Price || agg == 0.0 ? next.Price : agg),
                    StoresNumber = x.StorePrices.GroupBy(x => x.Shop).Count()
                })
                .OrderBy(x => x.Country);
        }

        #endregion
    }
}
